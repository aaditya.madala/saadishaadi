import 'jquery';
import 'bootstrap';
import 'fullpage.js';

$(document).ready(function () {
  $('#fullpage').fullpage({
    slidesNavigation: true,
    controlArrows: false,
    autoScrolling: false,
    fitToSection: false,
    afterRender: function () {
      setInterval(function () {
        $.fn.fullpage.moveSlideRight();
      }, 5000);
    }
  });
});
